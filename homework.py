#!/usr/bin/env python3

__author__ = "Mateusz Garbiak"

import random
liczba = random.randint(0,100)

print("Wylosowalem liczbe z zakresu 0...100")
strzal=0
proba = 0

while strzal != liczba: 
    proba+=1
    print("\nZgadnij liczbe - To twoja ", proba, " proba") 
    strzal = input()
    strzal = int(strzal)
    if strzal == liczba:
        print("Brawo. Zgadles w ", proba," probie")
    elif strzal < liczba:
        print( "Twoja liczba jest za mala")
    elif strzal > liczba:
        print( "Twoja liczba jest za duza")